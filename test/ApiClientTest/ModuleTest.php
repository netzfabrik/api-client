<?php
namespace ApiClientTest;

use ApiClient\Module;
use PHPUnit\Framework\TestCase;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Tests for the Module
 * @author heik
 * @covers ApiClient\Module
 */
class ModuleTest extends TestCase
{
    /**
     * Test module interfaces
     */
    public function testINterface()
    {
        $module = new Module();
        $this->assertInstanceOf(ConfigProviderInterface::class, $module);
    }

    /**
     * Test getConfig returns array
     */
    public function testGetConfig()
    {
        $module = new Module();
        $this->assertIsArray($module->getConfig());
    }
}
