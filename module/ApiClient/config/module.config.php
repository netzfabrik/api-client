<?php
namespace ApiClient;

use ApiClient\Client\ApiClient;
use ApiClient\Listener\CacheListener;
use ApiClient\Listener\DefaultListener;
use ApiClient\Service\ApiClientService;
use ApiClient\Service\CacheListenerFactory;
use Zend\EventManager\EventManager;
use Zend\ServiceManager\Factory\InvokableFactory;
use ApiClient\Service\DefaultListenerFactory;

return [
    'service_manager' => [
        'factories' => [
            ApiClientService::class => Service\ApiClientServiceFactory::class,
            ApiClient::class => InvokableFactory::class,
            // Eventmanager and listener
            EventManager::class => InvokableFactory::class,
            DefaultListener::class => DefaultListenerFactory::class,
            CacheListener::class => CacheListenerFactory::class,
        ]
    ],
    // module config
    'api_client' => [
        'client' => Client\ApiClient::class,
        'header' => [
            'user-agent' => 'api-client-lib'
        ],
        'event_manager' => EventManager::class,
        'cache_interface' => false, // provide reference to instance of CacheInterface in service_manager
        'listener' => [
            'default' => DefaultListener::class,
            'cache'   => CacheListener::class
        ],
    ]
];
