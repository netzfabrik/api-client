<?php
namespace ApiClient\Request;

use Zend\Http\Request;

/**
 * Interface for request entities
 * @author heik
 */
interface RequestInterface
{
    /**
     * Get HTTP Request
     * @return Request
     */
    public function getRequest();
}
