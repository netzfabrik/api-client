<?php
namespace ApiClient;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Module for the ApiClient
 * @author heik
 */
class Module implements ConfigProviderInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
