<?php
namespace ApiClient\Listener;

use ApiClient\Cache\CacheInterface;
use ApiClient\Events\ExecuteRequestEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

/**
 * Cache listener intercepting the ExecuteRequestEvent
 * @author heik
 */
class CacheListener implements ListenerAggregateInterface
{
    /**
     * @var CacheInterface
     */
    private $cacheInterface;

    /**
     * @param CacheInterface $cacheInterface
     */
    public function __construct(CacheInterface $cacheInterface)
    {
        $this->cacheInterface = $cacheInterface;
    }

    /**
     * {@inheritDoc}
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->attach(ExecuteRequestEvent::NAME, [$this, 'executeRequest'], 10);
    }

    /**
     * {@inheritDoc}
     * @see \Zend\EventManager\ListenerAggregateInterface::detach()
     */
    public function detach(EventManagerInterface $events)
    {
        $events->detach([$this, 'executeRequest'], '*');
    }

    /**
     * Listener on ExecuteRequestEvent
     * @param ExecuteRequestEvent $executeEvent
     */
    public function executeRequest(ExecuteRequestEvent $executeEvent)
    {
        $request = $executeEvent->getRequest();
        $cacheKey = md5($request->toString());
        if ($result = $this->cacheInterface->get($cacheKey)) {
            $executeEvent->setResult($result);
            $executeEvent->stopPropagation();
        }
    }
}
