<?php
namespace ApiClient\Service;

use ApiClient\Listener\DefaultListener;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the DefaultListener
 * @author heik
 */
class DefaultListenerFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $apiClientConfig = $config['api_client'];

        // additional request headers
        $headers = $apiClientConfig['header'];
        return new DefaultListener($headers);
    }
}
