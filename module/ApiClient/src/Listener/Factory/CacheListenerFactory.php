<?php
namespace ApiClient\Service;

use ApiClient\Cache\CacheInterface;
use ApiClient\Listener\CacheListener;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the CacheListener
 * @author heik
 */
class CacheListenerFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $apiClientConfig = $config['api_client'];

        // init cache interface - if any
        $cacheInterfaceClass = $apiClientConfig['cache_interface'];
        if ($cacheInterface = $container->get($cacheInterfaceClass) instanceof CacheInterface) {
            return new CacheListener($cacheInterface);
        }
    }
}
