<?php
namespace ApiClient\Listener;

use ApiClient\Events\ExecuteRequestEvent;
use ApiClient\Events\PrepareRequestEvent;
use ApiClient\Events\ProcessResultEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Response;
use Zend\Json\Json;

/**
 * Default listener for ApiClient to prepare headers, execute and handle JSON reponse
 * @author heik
 */
class DefaultListener implements ListenerAggregateInterface
{
    /**
     * @var array
     */
    private $headers;

    /**
     * @param array $headers
     */
    public function __construct(array $headers = [])
    {
        $this->headers = $headers;
    }

    /**
     * {@inheritDoc}
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->attach(PrepareRequestEvent::NAME, [$this, 'prepareRequest']);
        $events->attach(ExecuteRequestEvent::NAME, [$this, 'executeRequest']);
        $events->attach(ProcessResultEvent::NAME, [$this, 'processResult']);
    }

    /**
     * {@inheritDoc}
     * @see \Zend\EventManager\ListenerAggregateInterface::detach()
     */
    public function detach(EventManagerInterface $events)
    {
        $events->detach([$this, 'prepareRequest'], '*');
        $events->detach([$this, 'executeRequest'], '*');
        $events->detach([$this, 'processResult'], '*');
    }

    /**
     * Listener on PrepareRequestEvent
     * @param PrepareRequestEvent $prepareEvent
     */
    public function prepareRequest(PrepareRequestEvent $prepareEvent)
    {
        // prepare request headers
        $request = $prepareEvent->getRequest();
        $headers = $request->getHeaders();
        foreach ($this->headers as $headerName => $headerValue) {
            $headers->addHeaderLine($headerName, $headerValue);
        }
    }

    /**
     * Listener on ExecuteRequestEvent
     * @param ExecuteRequestEvent $executeEvent
     */
    public function executeRequest(ExecuteRequestEvent $executeEvent)
    {
        $client = $executeEvent->getClient();
        $request = $executeEvent->getRequest();

        $result = $client->send($request);
        $executeEvent->setResult($result);
    }

    /**
     * Listener on ProcessResultEvent
     * @param ProcessResultEvent $processResultEvent
     */
    public function processResult(ProcessResultEvent $processResultEvent)
    {
        $result = $processResultEvent->getExecutionResult();
        if (!$result instanceof Response) {
            return;
        }

        // decode JSON repsponse
        $body = $result->getBody();
        if ($result->getHeaders()->get('Content-Type') == 'application/json') {
            $body = Json::decode($body, Json::TYPE_ARRAY);
        }

        // set reponse body as result
        $processResultEvent->setResult($body);
    }
}
