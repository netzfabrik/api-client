<?php
namespace ApiClient\Service;

use ApiClient\Client\ApiClient;
use Interop\Container\ContainerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the ApiClientService
 * @author heik
 */
class ApiClientServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $apiClientConfig = $config['api_client'];

        // init http client
        $httpClientClass = $apiClientConfig['client'];
        if (!$client = $container->get($httpClientClass)) {
            $client = new ApiClient();
        }

        // init eventmanager
        $eventManagerClass = $apiClientConfig['event_manager'];
        if (!$eventManager = $container->get($eventManagerClass)) {
            $eventManager = new EventManager();
        }

        // attach listeners - provided via config, required to be a
        // ListenerAggregateInterface and defined in service_manager
        $listener = $apiClientConfig['listener'];
        foreach ($listener as $listenerClass) {
            if ($listener = $container->get($listenerClass) && $listener instanceof ListenerAggregateInterface) {
                $listener->attach($eventManager);
            }
        }

        return new ApiClientService($client, $eventManager);
    }
}
