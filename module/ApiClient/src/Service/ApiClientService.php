<?php
namespace ApiClient\Service;

use ApiClient\Events\ExecuteRequestEvent;
use ApiClient\Events\PrepareRequestEvent;
use ApiClient\Events\ProcessResultEvent;
use ApiClient\Request\RequestInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Http\Client;

/**
 * ApiClient service for request handling
 * @author heik
 */
class ApiClientService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var EventManagerInterface
     */
    private $eventManager;

    /**
     * @param Client $client
     * @param EventManagerInterface $eventManager
     */
    public function __construct(Client $client, EventManagerInterface $eventManager)
    {
        $this->client = $client;
        $this->eventManager = $eventManager;
    }

    /**
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        return $this->eventManager;
    }

    /**
     * Execute request
     * @param RequestInterface $requestInterface
     */
    public function execute(RequestInterface $requestInterface)
    {
        // --- Prepare
        $prepareEvent = new PrepareRequestEvent($requestInterface, $this->client);
        $this->eventManager->triggerEvent($prepareEvent);

        // --- Execute
        $executeEvent = new ExecuteRequestEvent($requestInterface, $this->client);
        $this->eventManager->triggerEvent($executeEvent);

        // --- Process
        $processResultEvent = new ProcessResultEvent($executeEvent->getResult());
        $this->eventManager->triggerEvent($processResultEvent);

        return $processResultEvent->getResult();
    }
}
