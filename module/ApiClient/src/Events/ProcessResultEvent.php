<?php
namespace ApiClient\Events;

use Zend\EventManager\Event;

/**
 * Event for request preaparation
 * @author heik
 */
class ProcessResultEvent extends Event
{
    const NAME = 'handle-result';

    /**
     * @var mixed
     */
    private $result;

    /**
     * Create event
     */
    public function __construct($result)
    {
        parent::__construct(self::NAME, $result);
    }

    /**
     * @return mixed
     */
    public function getExecutionResult()
    {
        return $this->getTarget();
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
}
