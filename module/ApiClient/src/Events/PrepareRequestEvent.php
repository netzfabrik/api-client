<?php
namespace ApiClient\Events;

use ApiClient\Request\RequestInterface;
use Zend\Http\Client;

/**
 * Event for request preaparation
 * @author heik
 */
class PrepareRequestEvent extends AbstractRequestEvent
{
    const NAME = 'prepare-request';

    /**
     * @param RequestInterface $requestInterface
     * @param Client $client
     */
    public function __construct(RequestInterface $requestInterface, Client $client)
    {
        parent::__construct(self::NAME, $requestInterface, $client);
    }
}
