<?php
namespace ApiClient\Events;

use ApiClient\Request\RequestInterface;
use Zend\EventManager\Event;
use Zend\Http\Client;
use Zend\Http\Request;

/**
 * Abstract Event for requests
 * @author heik
 */
abstract class AbstractRequestEvent extends Event
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param string $name
     * @param RequestInterface $requestInterface
     * @param Client $client
     */
    public function __construct(string $name, RequestInterface $requestInterface, Client $client)
    {
        parent::__construct($name, $requestInterface);
        $this->client = $client;
    }

    /**
     * @return RequestInterface
     */
    public function getRequestInterface()
    {
        return $this->getTarget();
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->getRequestInterface()->getRequest();
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
