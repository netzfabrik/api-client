<?php
namespace ApiClient\Events;

use ApiClient\Request\RequestInterface;
use Zend\Http\Client;

/**
 * Event for request preaparation
 * @author heik
 */
class ExecuteRequestEvent extends AbstractRequestEvent
{
    const NAME = 'execute-request';

    /**
     * @var mixed
     */
    private $result;

    /**
     * @param RequestInterface $requestInterface
     * @param Client $client
     */
    public function __construct(RequestInterface $requestInterface, Client $client)
    {
        parent::__construct(self::NAME, $requestInterface, $client);
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
}
