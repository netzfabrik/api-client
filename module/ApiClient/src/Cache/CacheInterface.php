<?php
namespace ApiClient\Cache;

/**
 * Caching interface
 * @author heik
 */
interface CacheInterface
{
    /**
     * Cache entry exists?
     * @param string $key
     * @return bool
     */
    public function has(string $key);

    /**
     * Add cache entry
     * @param string $key
     * @param mixed $value
     */
    public function add(string $key, $value);

    /**
     * Get cache entry
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * Remove cache entry
     * @param string $key
     */
    public function delete(string $key);
}
